package com.example.searchjobs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SearchJobsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SearchJobsApplication.class, args);
    }

}
